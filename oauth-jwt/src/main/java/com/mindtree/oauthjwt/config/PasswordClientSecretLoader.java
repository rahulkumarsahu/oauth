package com.mindtree.oauthjwt.config;

import org.springframework.context.annotation.Bean;

import com.mindtree.oauthjwt.util.UgmtClientCredentials;

public class PasswordClientSecretLoader {
	
	@Bean
	public UgmtClientCredentials ugmtClientCredentials() {
		return new UgmtClientCredentials("web", "pass");
	}
	
}
