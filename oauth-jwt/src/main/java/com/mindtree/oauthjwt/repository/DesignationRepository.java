package com.mindtree.oauthjwt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.oauthjwt.domain.Designation;



@Repository(value = "jpaDesignationRepository")
public interface DesignationRepository extends JpaRepository<Designation, Integer> {

	Designation findByCode(String string);

	List<Designation> findByIdIn(List<Integer> designationIds);

	List<Designation> findAllByOrderByIdAsc();

	Designation findByName(String string);

}
