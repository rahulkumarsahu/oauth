package com.mindtree.oauthjwt.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rahul
 *
 * This class is added to provide oauth2 client Id and secret dynamically.
 * User is responsible to make its bean and pass client Id and secret in the application.
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UgmtClientCredentials {

	private String clientId;

	private String clientPassword;
	
}
