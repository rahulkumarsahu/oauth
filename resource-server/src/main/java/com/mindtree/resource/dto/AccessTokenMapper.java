package com.mindtree.resource.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AccessTokenMapper {

	private String userId;
	private List<String> authorities = new ArrayList<String>();
	private String firstName;
	private String lastName;
	private String userType;
	private String mobile;
	private String country;
	
}
