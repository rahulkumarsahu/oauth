package com.mindtree.resource.controller;

import static com.mindtree.resource.constants.AuthConstants.ROLE_CONTROLLER;
import static com.mindtree.resource.constants.AuthConstants.ROLE_URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.resource.dao.RoleDao;
import com.mindtree.resource.service.RoleService;

@RestController(ROLE_CONTROLLER)
@RequestMapping(ROLE_URI)
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@GetMapping
	public RoleDao listRole(Pageable pageable) {
		return roleService.listRole(pageable);
	}
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@GetMapping(value = "/{role-id}")
	public RoleDao getRole(@PathVariable("role-id") Integer roleId) {
		return roleService.getRole(roleId);
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@PostMapping
	public RoleDao addRole(@RequestBody @Valid RoleDao roleDao) {
		return roleService.addRole(roleDao);
	}
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@PatchMapping(value = "/{role-id}")
	public RoleDao updateRole(@PathVariable("role-id") Integer roleId,
			@RequestBody @Valid RoleDao roleDao) {
		return roleService.updateRole(roleId, roleDao);
	}


}
