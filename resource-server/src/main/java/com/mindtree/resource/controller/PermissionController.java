package com.mindtree.resource.controller;

import static com.mindtree.resource.constants.AuthConstants.PERMISSION_URI;

import java.util.ArrayList;
import java.util.List;

import static com.mindtree.resource.constants.AuthConstants.PERMISSION_CONTROLLER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.resource.dto.AccessTokenMapper;
import com.mindtree.resource.dto.Permission;
import com.mindtree.resource.service.PermissionService;


@RestController(PERMISSION_CONTROLLER)
@RequestMapping(PERMISSION_URI)
public class PermissionController {
	
	@Autowired
	PermissionService permissionService;
	
	@GetMapping	
	@PreAuthorize("hasAnyRole('view_permission', 'SUPERADMIN')")
	public List<Permission> getListOfPermissions() {
		
		AccessTokenMapper accessTokenMapper = (AccessTokenMapper) 
				((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getDecodedDetails();
		
		System.out.println("accessTokenMapper.getFirst_name() :: " + accessTokenMapper.getFirstName());
		System.out.println("accessTokenMapper.getLast_name() :: " + accessTokenMapper.getLastName());
		
		return permissionService.getListOfPermissions();
		
	}
	
	@PreAuthorize("hasAnyRole('view_permissions_by_role', 'SUPERADMIN')")
	@GetMapping(value = "/roles/{id}")
	public List<String> viewPermissionsByRole(@PathVariable("id") String role_id) {
		return permissionService.getViewPermissionsByRole(role_id);
	}
	
	@PreAuthorize("hasAnyRole('assign_permissions_to_role', 'SUPERADMIN')")
	@PutMapping(value = "/roles/{id}")
	public ResponseEntity<Object> assignPermissions2Role(@PathVariable("id") String role_id, @RequestBody ArrayList<String> permissionsList) {
		permissionService.assignPermissions2Role(role_id, permissionsList);
		return new ResponseEntity<>("Permissions are assigned to role successfully", HttpStatus.OK);
	}
	
}
