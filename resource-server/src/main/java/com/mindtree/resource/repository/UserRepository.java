package com.mindtree.resource.repository;

import static com.mindtree.resource.constants.AuthConstants.USER_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.resource.dao.UserDao;

@Repository(USER_REPOSITORY)
public interface UserRepository extends JpaRepository<UserDao, Integer> {


}
