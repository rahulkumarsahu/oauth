package com.mindtree.resource.repository;

import static com.mindtree.resource.constants.AuthConstants.ROLE_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.resource.dao.RoleDao;

@Repository(ROLE_REPOSITORY)
public interface RoleRepository extends JpaRepository<RoleDao, Integer> {

}
