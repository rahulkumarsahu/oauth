package com.mindtree.resource.repository;

import static com.mindtree.resource.constants.AuthConstants.PERMISSION_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.resource.dao.PermissionDao;

@Repository(PERMISSION_REPOSITORY)
public interface PermissionRepository extends JpaRepository<PermissionDao, Integer>  {
	
	

}
