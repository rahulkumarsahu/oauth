package com.mindtree.resource.repository;

import static com.mindtree.resource.constants.AuthConstants.ROLE_USER_MAPPING_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.resource.dao.RoleUserMappingDao;

@Repository(ROLE_USER_MAPPING_REPOSITORY)
public interface RoleUserMappingRepository extends JpaRepository<RoleUserMappingDao, Integer>{

}
