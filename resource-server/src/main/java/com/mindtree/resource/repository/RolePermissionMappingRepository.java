package com.mindtree.resource.repository;

import static com.mindtree.resource.constants.AuthConstants.ROLE_PERMISSION_MAPPING_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.resource.dao.RolePermissionMappingDao;

@Repository(ROLE_PERMISSION_MAPPING_REPOSITORY)
public interface RolePermissionMappingRepository extends JpaRepository<RolePermissionMappingDao, Integer>{

}
