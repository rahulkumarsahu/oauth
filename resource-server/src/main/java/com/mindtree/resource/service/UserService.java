package com.mindtree.resource.service;

import static com.mindtree.resource.constants.AuthConstants.USER_SERVICE;


import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mindtree.resource.dao.UserDao;

@Service(USER_SERVICE)
public interface UserService {
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	UserDao listUser(Pageable pageable);

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	UserDao getUser(String emailId);

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	UserDao addUser(UserDao userDao);

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	UserDao updateUser(String emailId, UserDao userDao);
	
	

}
