package com.mindtree.resource.service;

import static com.mindtree.resource.constants.AuthConstants.PERMISSION_SERVICE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import com.mindtree.resource.dto.Permission;


@Service(PERMISSION_SERVICE)
public interface PermissionService {

	List<Permission> getListOfPermissions();

	List<String> getViewPermissionsByRole(String role_id);

	void assignPermissions2Role(String role_id, ArrayList<String> permissionsList);

}
