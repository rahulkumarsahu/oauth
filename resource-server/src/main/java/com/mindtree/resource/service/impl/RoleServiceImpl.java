package com.mindtree.resource.service.impl;

import static com.mindtree.resource.constants.AuthConstants.ROLE_SERVICE_IMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.resource.dao.RoleDao;
import com.mindtree.resource.repository.RolePermissionMappingRepository;
import com.mindtree.resource.repository.RoleRepository;
import com.mindtree.resource.repository.RoleUserMappingRepository;
import com.mindtree.resource.service.RoleService;

@Service(ROLE_SERVICE_IMPL)
@Transactional
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	RoleUserMappingRepository roleUserMappingRepository;
	
	@Autowired
	RolePermissionMappingRepository rolePermissionMappingRepository;

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao listRole(Pageable pageable) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao getRole(Integer roleId) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao addRole(RoleDao roleDao) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao updateRole(Integer roleId, RoleDao roleDao) {
		return null;
	}

}
