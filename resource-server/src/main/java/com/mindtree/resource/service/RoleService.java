package com.mindtree.resource.service;

import static com.mindtree.resource.constants.AuthConstants.ROLE_SERVICE;


import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mindtree.resource.dao.RoleDao;

@Service(ROLE_SERVICE)
public interface RoleService {

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	RoleDao listRole(Pageable pageable);

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	RoleDao getRole(Integer roleId);

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	RoleDao addRole(RoleDao roleDao);

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	RoleDao updateRole(Integer roleId, RoleDao roleDao);

}
