package com.mindtree.resource.service.impl;

import static com.mindtree.resource.constants.AuthConstants.PERMISSION_SERVICE_IMPL;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.resource.dao.PermissionDao;
import com.mindtree.resource.dto.Permission;
import com.mindtree.resource.repository.PermissionRepository;
import com.mindtree.resource.service.PermissionService;


@Service(PERMISSION_SERVICE_IMPL)
@Transactional
public class PermissionServiceImpl implements PermissionService {
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<Permission> getListOfPermissions() {
		
		List<PermissionDao> permissionsDaoList = permissionRepository.findAll();
		List<Permission> permissionList = new ArrayList<>();
		
		permissionsDaoList.forEach(permissionData -> {
			Permission permission = modelMapper.map(permissionData, Permission.class);
			permissionList.add(permission);
		});
		
		return permissionList;
	}

	@Override
	public List<String> getViewPermissionsByRole(String role_id) {
		return null;
	}

	@Override
	public void assignPermissions2Role(String role_id, ArrayList<String> permissionsList) {
		
	}
}
	
