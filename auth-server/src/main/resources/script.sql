/**
User Service
**/
drop database if exists userservice;
create database userservice;
use userservice;

/*
Create permission Table. pk -> id.
*/
create table permission (permission_id int primary key auto_increment, permission_name varchar(50) unique key not null);

/*
Create role Table. pk -> id.
*/
create table role (role_id int primary key auto_increment, role_name varchar(50) unique key not null);

/*
Create role_permission Table. One Permission Has Many Roles. or Many Roles can have one permission. 
pk -> id, fk -> role_id from role table and fk -> permission_id from permission table.
*/
create table role_permission(role_permission_id int primary key auto_increment, role_id int, foreign key(role_id) 
references role(role_id),permission_id int, foreign key(permission_id) references permission(permission_id));

/*
Create users Table. 
*/
create table users (user_id int primary key auto_increment, first_name varchar(50) not null, last_name varchar(50),
email_id varchar(50) not null, password varchar(1000), mobile varchar(20), country varchar(50), 
user_type varchar(20));

/*
Create role_users table. one role can have many users. or Many user can have on role.
*/
create table role_users (role_users_id int primary key auto_increment, role_id int, foreign key(role_id) references role(role_id),
user_id int, foreign key(user_id) references users(user_id));

/*
Permissions Values
*/
insert into permission (permission_name) values ('view_permission');

insert into permission (permission_name) values ('create_role');
insert into permission (permission_name) values ('edit_role');
insert into permission (permission_name) values ('view_role');
insert into permission (permission_name) values ('delete_role');

insert into permission (permission_name) values ('assign_permissions_to_role');
insert into permission (permission_name) values ('view_permissions_by_role');

insert into permission (permission_name) values ('create_users');
insert into permission (permission_name) values ('edit_users');
insert into permission (permission_name) values ('view_users');
insert into permission (permission_name) values ('delete_users');

insert into permission (permission_name) values ('assign_users_to_role');
insert into permission (permission_name) values ('view_users_by_role');

/*
Role Values
*/
insert into role (role_name) values ('Administrator');

/*
Role_Permissions Values
*/
insert into role_permission (role_id, permission_id) values (1,1); 
insert into role_permission (role_id, permission_id) values (1,2); 
insert into role_permission (role_id, permission_id) values (1,3); 
insert into role_permission (role_id, permission_id) values (1,4); 


insert into role_permission (role_id, permission_id) values (1,5); 
insert into role_permission (role_id, permission_id) values (1,6); 
insert into role_permission (role_id, permission_id) values (1,7); 
insert into role_permission (role_id, permission_id) values (1,8); 

insert into role_permission (role_id, permission_id) values (1,9); 
insert into role_permission (role_id, permission_id) values (1,10); 
insert into role_permission (role_id, permission_id) values (1,11); 
insert into role_permission (role_id, permission_id) values (1,12); 

insert into role_permission (role_id, permission_id) values (1,13); 

/*
User Values
*/
insert into users (first_name, last_name, email_id, password, mobile, country, user_type) values 
('Rahul', 'Kumar', 'rahul_k@gmail.com','$2y$12$U9VFvF/e4XtvXv8qQn7GmOOlAOnXAb0Al.qTOvbI3Gi0yMyiqaYNW',
'+919977646810','IND', 'super_admin');

insert into users (first_name, last_name, email_id, password, mobile, country, user_type) values 
('Shubham', 'Sinha', 'shubham_s@gmail.com','$2y$12$U9VFvF/e4XtvXv8qQn7GmOOlAOnXAb0Al.qTOvbI3Gi0yMyiqaYNW','+918939052159','IND','admin');

/*
Role_User Values
*/
insert into role_users (role_id,user_id) values (1,2);

select * from users where email_id='rahul_k@gmail.com';
select * from users where email_id='shubham_s@gmail.com';

/*
getting the permission name
*/
select p.permission_name from users u
inner join role_users r_u on u.user_id=r_u.user_id
inner join role r on r_u.role_id=r.role_id
inner join role_permission r_p on r_p.role_id=r.role_id
inner join permission p on p.permission_id=r_p.permission_id where u.email_id='shubham_s@gmail.com';

update users set password='$2a$10$eUB5vV/xWXrcR61TqcWRXOCfl37r3fk.UXNXEzx7xTol5inz5xZ.2';