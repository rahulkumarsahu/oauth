package com.mindtree.auth.dto;

import org.springframework.security.core.userdetails.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CustomUser extends User {
	
	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String firstName;
	private String lastName;
	private String mobile;
	private String country;
	private String userType;
	

	public CustomUser(UserEntity user) {
		super(user.getEmailId(), user.getPassword(), user.getGrantedAuthoritiesList());
		this.userId = user.getUserId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.mobile = user.getMobile();
		this.country = user.getCountry();
		this.userType = user.getUserType();
	}

}
