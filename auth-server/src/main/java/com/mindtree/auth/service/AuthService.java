package com.mindtree.auth.service;

import static com.mindtree.auth.constants.AuthConstants.AUTH_SERVICE;

import org.springframework.stereotype.Service;

import com.mindtree.auth.dto.UserEntity;

@Service(AUTH_SERVICE)
public interface AuthService {

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	UserEntity getUserDetails(String emailId);

}
