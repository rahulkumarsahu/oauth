package com.mindtree.auth.service.impl;

import static com.mindtree.auth.constants.AuthConstants.ROLE_SERVICE_IMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.auth.dao.RoleDao;
import com.mindtree.auth.repository.RolePermissionMappingRepository;
import com.mindtree.auth.repository.RoleRepository;
import com.mindtree.auth.repository.RoleUserMappingRepository;
import com.mindtree.auth.service.RoleService;

@Service(ROLE_SERVICE_IMPL)
@Transactional
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	RoleUserMappingRepository roleUserMappingRepository;
	
	@Autowired
	RolePermissionMappingRepository rolePermissionMappingRepository;

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao listRole(Pageable pageable) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao getRole(Integer roleId) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao addRole(RoleDao roleDao) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public RoleDao updateRole(Integer roleId, RoleDao roleDao) {
		return null;
	}

}
