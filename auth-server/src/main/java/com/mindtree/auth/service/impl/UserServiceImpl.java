package com.mindtree.auth.service.impl;

import static com.mindtree.auth.constants.AuthConstants.USER_SERVICE_IMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.auth.dao.UserDao;
import com.mindtree.auth.repository.UserRepository;
import com.mindtree.auth.service.UserService;

@Service(USER_SERVICE_IMPL)
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepository userRepository;

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public UserDao listUser(Pageable pageable) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public UserDao getUser(String emailId) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public UserDao addUser(UserDao userDao) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public UserDao updateUser(String emailId, UserDao userDao) {
		return null;
	}

}
