package com.mindtree.auth.service.impl;

import static com.mindtree.auth.constants.AuthConstants.AUTH_SERVICE_IMPL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.auth.dao.UserDao;
import com.mindtree.auth.dto.UserEntity;
import com.mindtree.auth.repository.PermissionRepository;
import com.mindtree.auth.repository.UserRepository;
import com.mindtree.auth.service.AuthService;

@Service(AUTH_SERVICE_IMPL)
@Transactional
public class AuthServiceImpl implements AuthService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@Autowired
	ModelMapper modelMapper;

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public UserEntity getUserDetails(String emailId) {
		
		Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
		
		UserDao userDao = userRepository.findByEmailId(emailId);
		UserEntity user = modelMapper.map(userDao, UserEntity.class);
		
		if (user.getUserType() != null && !user.getUserType().trim().equalsIgnoreCase("super_admin")) {
			
			List<String> permissionList = permissionRepository.getAllPermissionByEmailId(emailId);
			List<String> permissions = permissionList.parallelStream().map(permission -> "ROLE_" + permission).collect(Collectors.toList());
			
			if (permissions != null && !permissions.isEmpty()) {
				for (String permission : permissionList) {
					GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(permission);
					grantedAuthoritiesList.add(grantedAuthority);
				}
				user.setGrantedAuthoritiesList(grantedAuthoritiesList);
			}
			return user;
				
		} else {
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_SUPERADMIN");
			grantedAuthoritiesList.add(grantedAuthority);
			user.setGrantedAuthoritiesList(grantedAuthoritiesList);
			return user;
		}
	}

}
