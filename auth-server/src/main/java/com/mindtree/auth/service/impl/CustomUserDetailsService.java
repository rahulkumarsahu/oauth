package com.mindtree.auth.service.impl;
import static com.mindtree.auth.constants.AuthConstants.CUSTOM_USER_SERVICE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mindtree.auth.dto.CustomUser;
import com.mindtree.auth.dto.UserEntity;
import com.mindtree.auth.service.AuthService;

@Service(CUSTOM_USER_SERVICE)
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	AuthService authService;

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {
		
		UserEntity userEntity = null;
		try {
			userEntity = authService.getUserDetails(emailId);
			if (userEntity != null && userEntity.getUserId() != null && !"".equalsIgnoreCase(userEntity.getUserId())) {
				CustomUser customUser = new CustomUser(userEntity);
				return customUser;
			} else {
				throw new UsernameNotFoundException("User " + emailId + " was not found in the database");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsernameNotFoundException("User " + emailId + " was not found in the database");
		}
	}

}
