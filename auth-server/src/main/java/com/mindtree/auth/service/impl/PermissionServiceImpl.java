package com.mindtree.auth.service.impl;

import static com.mindtree.auth.constants.AuthConstants.PERMISSION_SERVICE_IMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.auth.dao.PermissionDao;
import com.mindtree.auth.repository.PermissionRepository;
import com.mindtree.auth.service.PermissionService;

@Service(PERMISSION_SERVICE_IMPL)
@Transactional
public class PermissionServiceImpl implements PermissionService {
	
	@Autowired
    PermissionRepository permissionRepository;
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public PermissionDao listPermission(Pageable pageable) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public PermissionDao getPermission(Integer permissionId) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public PermissionDao addPermission(PermissionDao permissionDao) {
		return null;
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@Override
	public PermissionDao updatePermission(Integer permissionId, PermissionDao permissionDao) {
		return null;
	}

}
