package com.mindtree.auth.controller;

import static com.mindtree.auth.constants.AuthConstants.USER_CONTROLLER;
import static com.mindtree.auth.constants.AuthConstants.USER_URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.auth.dao.UserDao;
import com.mindtree.auth.service.UserService;

@RestController(USER_CONTROLLER)
@RequestMapping(USER_URI)
public class UserController {
	
	@Autowired
	private UserService userService;
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@GetMapping
	public UserDao listUser(Pageable pageable) {
		return userService.listUser(pageable);
	}
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@GetMapping(value = "/{email-id}")
	public UserDao getUser(@PathVariable("email-id") String emailId) {
		return userService.getUser(emailId);
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@PostMapping
	public UserDao addUser(@RequestBody @Valid UserDao userDao) {
		return userService.addUser(userDao);
	}
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@PatchMapping(value = "/{email-id}")
	public UserDao updateUser(@PathVariable("email-id") String emailId,
			@RequestBody @Valid UserDao userDao) {
		return userService.updateUser(emailId, userDao);
	}

}
