package com.mindtree.auth.controller;

import static com.mindtree.auth.constants.AuthConstants.PERMISSION_CONTROLLER;
import static com.mindtree.auth.constants.AuthConstants.PERMISSION_URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.auth.dao.PermissionDao;
import com.mindtree.auth.service.PermissionService;

@RestController(PERMISSION_CONTROLLER)
@RequestMapping(PERMISSION_URI)
public class PermissionController {
	
	@Autowired
	private PermissionService permissionService;
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@GetMapping
	public PermissionDao listPermission(Pageable pageable) {
		return permissionService.listPermission(pageable);
	}
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@GetMapping(value = "/{permission-id}")
	public PermissionDao getPermission(@PathVariable("permission-id") Integer permissionId) {
		return permissionService.getPermission(permissionId);
	}

	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@PostMapping
	public PermissionDao addConfig(@RequestBody @Valid PermissionDao permissionDao) {
		return permissionService.addPermission(permissionDao);
	}
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
	@PatchMapping(value = "/{permission-id}")
	public PermissionDao updateConfig(@PathVariable("permission-id") Integer permissionId,
			@RequestBody @Valid PermissionDao permissionDao) {
		return permissionService.updatePermission(permissionId, permissionDao);
	}
}
