package com.mindtree.auth.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.mindtree.auth.dto.CustomUser;

public class CustomTokenEnhancer extends JwtAccessTokenConverter {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		CustomUser user = (CustomUser) authentication.getPrincipal();
		Map<String, Object> info = new LinkedHashMap<>(accessToken.getAdditionalInformation());
		if (user.getUserId() != null)
			info.put("user_id", user.getUserId());
		if (user.getFirstName() != null)
			info.put("first_name", user.getFirstName());
		if (user.getLastName() != null)
			info.put("last_name", user.getLastName());
		if (user.getCountry() != null)
			info.put("country", user.getCountry());
		if (user.getMobile() != null)
			info.put("mobile", user.getMobile());
		if (user.getUserType() != null)
			info.put("user_type", user.getUserType());
		DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
		customAccessToken.setAdditionalInformation(info);
		return super.enhance(customAccessToken, authentication);
	}	
}