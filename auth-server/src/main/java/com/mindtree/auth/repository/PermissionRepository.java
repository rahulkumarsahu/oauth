package com.mindtree.auth.repository;

import static com.mindtree.auth.constants.AuthConstants.PERMISSION_REPOSITORY;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mindtree.auth.dao.PermissionDao;

@Repository(PERMISSION_REPOSITORY)
public interface PermissionRepository extends JpaRepository<PermissionDao, Integer>  {
	
	/**
	 * This method will.......
	 * 
	 * @param 
	 * @param 
	 * @return
	 */
    @Query(nativeQuery = true, value = "select p.permission_name from users u inner join role_users r_u on u.user_id=r_u.user_id" +
	                         " inner join role r on r_u.role_id=r.role_id inner join role_permission r_p on r_p.role_id=r.role_id"+
    		                 " inner join permission p on p.permission_id=r_p.permission_id where u.email_id=:emailId")
	List<String> getAllPermissionByEmailId(@Param("emailId") String emailId);

}
