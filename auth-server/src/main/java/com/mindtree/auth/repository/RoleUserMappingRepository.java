package com.mindtree.auth.repository;

import static com.mindtree.auth.constants.AuthConstants.ROLE_USER_MAPPING_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.auth.dao.RoleUserMappingDao;

@Repository(ROLE_USER_MAPPING_REPOSITORY)
public interface RoleUserMappingRepository extends JpaRepository<RoleUserMappingDao, Integer>{

}
