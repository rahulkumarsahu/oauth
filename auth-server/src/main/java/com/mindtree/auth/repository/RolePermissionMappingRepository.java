package com.mindtree.auth.repository;

import static com.mindtree.auth.constants.AuthConstants.ROLE_PERMISSION_MAPPING_REPOSITORY;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.auth.dao.RolePermissionMappingDao;

@Repository(ROLE_PERMISSION_MAPPING_REPOSITORY)
public interface RolePermissionMappingRepository extends JpaRepository<RolePermissionMappingDao, Integer>{

}
